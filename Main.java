public class Main {
    public static void main(String[] args) {
        Circle c1 = new Circle();
        System.out.println("Dairenin sahesi = " + c1.getArea() + " dairenin radisusu = "+ c1.getRadius());

        Circle c2 = new Circle(2.2);
        System.out.println("Dairenin sahesi = " + c2.getArea() + " dairenin radisusu = "+ c2.getRadius());

        Circle c3 = new Circle(5.5, "blue");
        System.out.println("Dairenin rengi : "+ c3.getColor());
        System.out.println("Dairenin radisusu: "+ c3.getRadius());
        System.out.println("Dairenin sahesi: " + c3.getArea());
        System.out.println(c3);
    }
}

